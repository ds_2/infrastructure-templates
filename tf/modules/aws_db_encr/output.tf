output "endpoint" {
    value=aws_db_instance.db.endpoint
}

output "arn" {
    value=aws_db_instance.db.arn
}
