variable "name" {
  type = string
}

variable "region" {
  type    = string
  default = "eu-central-1"
}

variable "acl" {
  type    = string
  default = "private"
}

variable "versioned" {
  type    = bool
  default = false
}

variable "readonlyIamArn" {
  type        = set(string)
  description = "the ARNs of the users who can only read data from the bucket"
  default     = []
}

variable "adminIamArn" {
  type        = set(string)
  description = "the ARNs of the users who can only read data from the bucket"
  default     = []
}

variable "maxUploadDays" {
  type    = number
  default = 3
}

variable "ncvDays" {
  type    = number
  default = 30
}

variable "ncvExpireDays" {
  type    = number
  default = 60
}

variable "kmsKeyArn" {
  type    = string
  default = null
}

variable "encryptContent" {
  type    = bool
  default = false
}

variable "isWebsite" {
  type    = bool
  default = false
}

variable "websiteIndexFile" {
  type    = string
  default = "index.html"
}

variable "websiteErrorFile" {
  type    = string
  default = "error.html"
}

variable "websiteRedirectAllTo" {
  type    = string
  default = null
}

variable "websiteRoutingRulesJson" {
  type    = string
  default = null
}
