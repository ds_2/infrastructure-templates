# Terraform modules

## How to add

Use

    module "aws1" {
        source = "git::git@gitlab.com:ds_2/infrastructure-templates.git//tf/modules/aws_s3_bucket"
        name   = "my-bucket"
    }

If a specific version/tag is required, use

    ?ref=v1.2.0

in the source url
